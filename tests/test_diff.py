"""Test diff parsing."""
from pathlib import Path

from lint_review.diff import changed_lines_in_multiple_hunks

FILE_DIR = Path(__file__).parent
RESOURCES = FILE_DIR / "resources"


def test_changed_lines_multiple_hunks():
    assert set(
        changed_lines_in_multiple_hunks(
            RESOURCES.joinpath("test_gitlab_diff.patch").read_text().splitlines()
        )
    ) == {9, *range(102, 109)}


def test_changed_lines_in_whole_file():
    assert set(
        changed_lines_in_multiple_hunks(
            RESOURCES.joinpath("test_lint_review_diff.patch").read_text().splitlines()
        )
    ) == {*range(1, 22)}
